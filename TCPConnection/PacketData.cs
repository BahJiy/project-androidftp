using System;
using System.Text;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace TCPConnection {
	public class PacketData {
		internal const int BUFFERSIZE = 1024;
		internal byte[ ] buffer;
		internal readonly List<byte> data;
		internal readonly Socket handler;

		public byte this[ int i ] {
			get { return data[ i ]; }
		}

		internal PacketData ( Socket handler ) {
			this.handler = handler;
			this.data = new List<byte> ( );
			this.buffer = new byte[ BUFFERSIZE ];
		}

		/// <summary>
		/// Get a packet's binary data
		/// </summary>
		/// <returns>The list of bytes in the packet</returns>
		public IReadOnlyList<byte> GetContentRaw ( ) { return data.AsReadOnly ( ); }

		/// <summary>
		/// Get the string representation of the packet data
		/// </summary>
		/// <param name="encoding">The type of encoding to use</param>
		/// <returns>The binary data as a stirng using the encoding provided</returns>
		public string GetContentString ( Encoding encoding ) { return encoding.GetString ( data.ToArray ( ) ); }

		/// <summary>
		/// Get the size of the packet
		/// </summary>
		/// <returns>The size of the packet</returns>
		public long GetSize ( ) { return data.Count; }
	}
}
