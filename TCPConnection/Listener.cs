﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;

namespace TCPConnection {
	/// <summary>
	/// A helper class to set up a TCP listener. Provides methods to handle the response to from the client
	/// </summary>
	public abstract class Listener {

		/// <summary>
		/// The main socket listener for the system. This is for accepting and sending request
		/// </summary>
		public readonly Socket sock;
		/// <summary>
		/// A lock on the main thread when we receive a connect so that we can handle it first
		/// </summary>
		private ManualResetEvent actReset = null;
		private bool Stop = false;

		public Listener ( ) => sock = new Socket ( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );

		/// <summary>
		/// Start the listener and wait for a request
		/// </summary>
		/// <param name="port">The port to listen on</param>
		public void StartListener ( int port ) {
			if ( actReset == null && !Stop ) {
				// Create the lock
				actReset = new ManualResetEvent ( false );
				// We want to async run this so the main thread doesn't block!
				Task.Run ( ( ) => {
					// Get our information
					IPHostEntry ipHostEntry = Dns.GetHostEntry ( Dns.GetHostName ( ) );
					IPAddress iPAddress = ipHostEntry.AddressList[ 0 ];
					IPEndPoint localEndPoint = new IPEndPoint ( iPAddress, port );

					try {
						// Set and bind the socket
						sock.Bind ( localEndPoint );
						sock.Listen ( 10 );

						// Just keep listening for connection request and handle them as needed
						while ( !Stop ) {
							// reset the lock
							actReset.Reset ( );

							Console.WriteLine ( "Waiting for a connection..." );
							sock.BeginAccept ( AcceptConnection, sock );

							// wait until we received a connection
							actReset.WaitOne ( );
						}
					} catch ( Exception e ) {
						Console.WriteLine ( e );
					}
				} );
			}
		}

		/// <summary>
		/// Stop the listener for receiving any more connection request
		/// </summary>
		public void StopListener ( ) {
			Stop = true;
			actReset.Set ( );
			actReset.Close ( );

			sock.Shutdown ( SocketShutdown.Both );
			sock.Close ( );
		}

		/// <summary>
		/// Accept an incoming request
		/// </summary>
		private void AcceptConnection ( IAsyncResult result ) {
			// Signal that we received a connection and the main thread can continue
			actReset.Set ( );

			// Accept the client request and get the socket to the client
			Socket listener = ( Socket ) result.AsyncState;
			Socket handler = listener.EndAccept ( result );

			PacketData packet = new PacketData ( handler );
			handler.BeginReceive ( packet.buffer, 0, PacketData.BUFFERSIZE, 0,
				new AsyncCallback ( Read ), packet );
		}

		/// <summary>
		/// Read the data the connection give
		/// </summary>
		/// <param name="result">The AsyncResult containing the PacketData to fill out</param>
		private void Read ( IAsyncResult result ) {
			PacketData packet = ( PacketData ) result.AsyncState;

			// End the current receive and get the number of bytes read
			int byteRead = packet.handler.EndReceive ( result );
			if ( byteRead > 0 ) {
				// Add the byte data to the list
				packet.data.AddRange ( packet.buffer );

				// This is to purely try to find the EOF file marker to signal that
				// we are done reading
				string content = Encoding.ASCII.GetString ( packet.buffer );
				if ( content.IndexOf ( "<EOF>" ) > -1 ) {
					// Everything was read...
					ReadCallback ( packet );
				} else {
					// We need to read more because EOF was not found
					packet.handler.BeginReceive ( packet.buffer, 0, PacketData.BUFFERSIZE, 0,
						new AsyncCallback ( Read ), packet );
				}

			}
		}

		/// <summary>
		/// Send data over to the receiver
		/// </summary>
		/// <param name="handler">The socket channel to the receiver</param>
		/// <param name="data">The string data to send</param>
		public void Send ( Socket handler, string data ) {
			byte[ ] byteData = Encoding.ASCII.GetBytes ( data );
			Send ( handler, byteData );
		}

		/// <summary>
		/// Send data over to the receiver
		/// </summary>
		/// <param name="handler">The socket channel to the receiver</param>
		/// <param name="data">The byte data to send</param>
		public void Send ( Socket handler, byte[ ] data ) =>
			handler.BeginSend ( data, 0, data.Length, 0,
				new AsyncCallback ( SendComplete ),
				Tuple.Create<Socket, byte[ ]> ( handler, data ) );

		/// <summary>
		/// Signal the completed send
		/// </summary>
		/// <param name="result">A tuple contain the handler and the data</param>
		public void SendComplete ( IAsyncResult result ) {
			Tuple<Socket, byte[ ]> data = ( Tuple<Socket, byte[ ]> ) result.AsyncState;

			int byteSent = data.Item1.EndSend ( result );

			SendCallback ( byteSent, data.Item2 );
		}

		/// <summary>
		/// The callback method for when a read is completed
		/// </summary>
		/// <param name="result">The PacketData containing the result from the client</param>
		public abstract void ReadCallback ( PacketData result );

		/// <summary>
		/// The callback method for when a send is completed
		/// </summary>
		/// <param name="byteSent">The number of byte sent</param>
		/// <param name="data">The data (in bytes) that was sent</param>
		public abstract void SendCallback ( int byteSent, byte[ ] data );
	}
}
