using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;

namespace TCPConnection {
	public static class IPHelper {
		private const string ipURI = @"https://ifconfig.co/";

		/// <summary>
		/// Get the local ip address
		/// </summary>
		/// <param name="port">The port to attempt to use</param>
		/// <returns>The local ip address</returns>
		public static String GetLocalIP ( int port = 8088 ) {
			using ( Socket socket = new Socket ( AddressFamily.InterNetwork, SocketType.Dgram, 0 ) ) {
				socket.Connect ( "8.8.8.8", port );
				IPEndPoint endPoint = ( IPEndPoint ) socket.LocalEndPoint;
				return endPoint.Address.ToString ( );
			}
		}

		/// <summary>
		/// Get the ip detail information
		/// </summary>
		/// <returns>The JObject containing the detail about the ip</returns>
		public static JObject GetPublicIPDetail ( ) {
			JObject reader;
			HttpWebRequest request = ( HttpWebRequest ) WebRequest.Create ( ipURI + "json" );
			using ( StreamReader response = new StreamReader ( request.GetResponse ( ).GetResponseStream ( ) ) ) {
				reader = JObject.Parse ( response.ReadToEnd ( ) );
			}

			return reader;
		}

		/// <summary>
		/// Get the public ip address
		/// </summary>
		/// <returns>The public ip address</returns>
		public static String GetPublicIP ( ) {
			HttpWebRequest request = ( HttpWebRequest ) WebRequest.Create ( ipURI + "ip" );
			using ( StreamReader response = new StreamReader ( request.GetResponse ( ).GetResponseStream ( ) ) ) {
				return response.ReadToEnd ( ).Trim ( );
			}
		}
	}
}
