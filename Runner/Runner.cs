﻿using System;
using TCPConnection;
using static TCPConnection.IPHelper;

namespace Runner {
	class Runner : Listener {
		static void Main ( string[ ] args ) {
			Console.WriteLine ( GetLocalIP ( ) );
			Console.WriteLine ( GetPublicIP ( ) );
			Console.WriteLine ( GetPublicIPDetail ( ).ToString ( ) );
		}

		public override void ReadCallback ( PacketData result ) {
			throw new NotImplementedException ( );
		}

		public override void SendCallback ( int byteSent, byte[ ] data ) {
			throw new NotImplementedException ( );
		}
	}
}
